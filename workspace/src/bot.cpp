#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <fstream>
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main(){
	int fd;
	DatosMemCompartida *pdatosCompartidos;
	
	//Abrir el fichero
	if((fd = open("/tmp/datosCompartidos",O_RDWR)) <0){
		perror("Error al abrir el fichero");
		exit(1);
	}
	
	//Proyectamos el fichero en memoria
	  //static_cast <new_type> (expresión), convertimos la expresión al tipo new_type
	  if((pdatosCompartidos = static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0))) == MAP_FAILED){
	  	perror("Error al proyectar el fichero");
	  	close(fd);
	  	return 1;
	  }
	  
	  //Cerramos el descriptor del fichero
	  close(fd);
	  
	  //Bucle infinito para establecer la acción del bot
	 while(1){
	  	float posRaqueta1;
	  	
	  	posRaqueta1 = ((pdatosCompartidos->raqueta1.y1 + pdatosCompartidos->raqueta1.y2)/2);
	  	
	  	if(pdatosCompartidos->esfera.centro.y < posRaqueta1)
	  		pdatosCompartidos->accion=-1; //Baja la raqueta
	  	else if(pdatosCompartidos->esfera.centro.y == posRaqueta1)
	  		pdatosCompartidos->accion=0; //No se mueve la raqueta
	  	else if(pdatosCompartidos->esfera.centro.y > posRaqueta1)
	  		pdatosCompartidos->accion=1; //Sube la raqueta
	  	
	  	usleep(25000); //Se suspende durante 25ms
	  }

	  //Desproyectamos
	//  munmap(pdatosCompartidos, sizeof(DatosMemCompartida));
	  
	  unlink("/tmp/datosCompartidos");
	  
	  return 1;
}
	
