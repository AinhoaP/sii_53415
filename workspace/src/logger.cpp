#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include "Puntuaciones.h"

int main(){
	int fd;
	char cadena[200];
	Puntuaciones puntos;
	
	//Creación FIFO
	if(mkfifo("/tmp/FIFO_LOGGER",0660) < 0){
		perror("No se puede crear el FIFO");
		return 1;
	}
	
	//Abrir FIFO
	if((fd = open("/tmp/FIFO_LOGGER",O_RDONLY)) < 0){
		perror("No se puede abrir el FIFO");
		return 1;
	}
	//Lectura e imprime por salida estándar
	while(read(fd, &puntos, sizeof(puntos)) == sizeof(puntos)){
		if(puntos.ganador == 1)
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador1);
	
		if(puntos.ganador == 2)
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador2);
	}

	//Destruir 
        close(fd);
        unlink("/tmp/FIFO_LOGGER");
        return (0);
}
